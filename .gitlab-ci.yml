# Pipelines to test code and build container images

stages:
  # Install testing tools into extra container to speed up later testing steps
  - container_testing
  - test
  - container_base
  - container_modules

# ==================================================================================================
# ===== Stage: Test-Container ======================================================================

# Use anchors to reuse parts for the different steps
.stage_container_anchor: &container_anchor
  image: "quay.io/buildah/stable"
  variables:
    # Use vfs with buildah. Docker offers overlayfs as a default, but buildah
    # cannot stack overlayfs on top of another overlayfs filesystem.
    STORAGE_DRIVER: "vfs"
    BUILDAH_FORMAT: "oci"
  before_script:
    # Print out program versions for debugging
    - buildah version
    - buildah login --username "${CI_REGISTRY_USER}" --password "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"

testing_image:
  <<: *container_anchor
  stage: container_testing
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    IMAGE_TAG: "testing_jaco_sat"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
    IMAGE_FILE: "tests/Containerfile"
  script:
    # Build image, using layers=false squashes only new layers,
    # but this reduces space usage while building and is much faster
    - echo "buildah bud --layers=false --squash -f ${IMAGE_FILE} -t ${IMAGE_TAG} ."
    - buildah bud --layers=false --squash -f ${IMAGE_FILE} -t ${IMAGE_TAG} .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch to reduce space usage
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - tests/Containerfile
      - .gitlab-ci.yml

# ==================================================================================================
# ===== Stage: Test ================================================================================

# Use anchors to reuse parts for the different steps
.stage_test_anchor: &test_anchor
  variables:
    IMAGE_TAG: "testing_jaco_sat"
    # Use the main repository's container image by default except for specific branch names
    FULL_IMAGE_NAME: "registry.gitlab.com/jaco-assistant/jaco-satellite/master/${IMAGE_TAG}"
  rules:
    - if: $CI_COMMIT_REF_SLUG == "master" || $CI_COMMIT_REF_SLUG == "develop"
      variables:
        FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
    - when: always
  image: ${FULL_IMAGE_NAME}

linting:
  <<: *test_anchor
  stage: test
  script:
    - isort --check-only --diff .
    - black --check --diff .
    - npx /usr/app/node_modules/prettylint/bin/cli.js \
      $(find . -type f \( -name '*.json' -o -name '*.md' -o -name '*.yaml' \) ! -path './.*/*')
    - pylint $(find . -type f -name "*.py" -not -path "./extras/*")
    - flake8 .
    - mypy .

analysis:
  <<: *test_anchor
  stage: test
  # The following pipe allows using a ':' in the 'sed' command, also remove the '-' list indicator
  script: |
    mkdir ./badges/
    radon cc -a . | tee ./badges/radon.log
    RCC_SCORE=$(sed -n 's/^Average complexity: \([A-F]\) .*/\1/p' ./badges/radon.log)
    anybadge --label=complexity --file=badges/rcc.svg --overwrite --value=$RCC_SCORE A=green B=yellow C=orange D=red E=red F=red
    echo "Radon cyclomatic complexity score is: $RCC_SCORE"
    pygount --folders-to-skip "venv,.*,node_modules" --format=summary ./
  artifacts:
    paths:
      - ./badges/

# ==================================================================================================
# ===== Stage: Container-Base ======================================================================

satellite_base_image_amd64:
  <<: *container_anchor
  stage: container_base
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    BASE_IMAGE_TAG: "satellite_base_image_amd64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${BASE_IMAGE_TAG}"
    IMAGE_FILE: "runfiles/Containerfile_SatelliteBase_amd64"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} .
    - buildah tag ${BASE_IMAGE_TAG} ${FULL_BASE_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch to reduce space usage
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_BASE_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - runfiles/Containerfile_SatelliteBase_amd64
      - .gitlab-ci.yml

satellite_base_image_arm64:
  <<: *container_anchor
  stage: container_base
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    BASE_IMAGE_TAG: "satellite_base_image_arm64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${BASE_IMAGE_TAG}"
    IMAGE_FILE: "runfiles/Containerfile_SatelliteBase_arm64"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} --platform=linux/arm64 .
    - buildah tag ${BASE_IMAGE_TAG} ${FULL_BASE_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch to reduce space usage
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_BASE_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - runfiles/Containerfile_SatelliteBase_arm64
      - .gitlab-ci.yml

satellite_base_image_armhf:
  <<: *container_anchor
  stage: container_base
  variables:
    # Set base image variables here because it didn't work to set them in the anchor
    BASE_IMAGE_TAG: "satellite_base_image_armhf"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${BASE_IMAGE_TAG}"
    IMAGE_FILE: "runfiles/Containerfile_SatelliteBase_armhf"
  script:
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} --platform=linux/arm ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${BASE_IMAGE_TAG} --platform=linux/arm .
    - buildah tag ${BASE_IMAGE_TAG} ${FULL_BASE_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch to reduce space usage
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_BASE_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - runfiles/Containerfile_SatelliteBase_armhf
      - .gitlab-ci.yml

# ==================================================================================================
# ===== Stage: Container-Modules ===================================================================

wake_word_amd64:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_amd64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "wake_word_amd64"
    IMAGE_FILE: "wake-word/Containerfile_amd64"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - wake-word/Containerfile_amd64
      - runfiles/Containerfile_SatelliteBase_amd64
      - .gitlab-ci.yml

wake_word_arm64:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_arm64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "wake_word_arm64"
    IMAGE_FILE: "wake-word/Containerfile_arm64"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - sed -i 's/FROM satellite_base_image_arm64/FROM localhost\/satellite_base_image_arm64/' wake-word/Containerfile_arm64
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm64 .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - wake-word/Containerfile_arm64
      - runfiles/Containerfile_SatelliteBase_arm64
      - .gitlab-ci.yml

wake_word_armhf:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_armhf"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "wake_word_armhf"
    IMAGE_FILE: "wake-word/Containerfile_armhf"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - sed -i 's/FROM satellite_base_image_armhf/FROM localhost\/satellite_base_image_armhf/' wake-word/Containerfile_armhf
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - wake-word/Containerfile_armhf
      - runfiles/Containerfile_SatelliteBase_armhf
      - .gitlab-ci.yml

audio_streamer_amd64:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_amd64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "audio_streamer_amd64"
    IMAGE_FILE: "audio-streamer/Containerfile_amd64"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - audio-streamer/Containerfile_amd64
      - runfiles/Containerfile_SatelliteBase_amd64
      - .gitlab-ci.yml

audio_streamer_arm64:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_arm64"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "audio_streamer_arm64"
    IMAGE_FILE: "audio-streamer/Containerfile_arm64"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - sed -i 's/FROM satellite_base_image_arm64/FROM localhost\/satellite_base_image_arm64/' audio-streamer/Containerfile_arm64
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm64 ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm64 .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - audio-streamer/Containerfile_arm64
      - runfiles/Containerfile_SatelliteBase_arm64
      - .gitlab-ci.yml

audio_streamer_armhf:
  <<: *container_anchor
  stage: container_modules
  variables:
    BASE_IMAGE_TAG: "satellite_base_image_armhf"
    FULL_BASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/master/${BASE_IMAGE_TAG}"
    IMAGE_TAG: "audio_streamer_armhf"
    IMAGE_FILE: "audio-streamer/Containerfile_armhf"
    FULL_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}/${IMAGE_TAG}"
  script:
    # Use image from master branch except if develop branch is used 
    - if [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then FULL_BASE_IMAGE_NAME="${CI_REGISTRY_IMAGE}/develop/${BASE_IMAGE_TAG}"; fi
    # Download base image from selected branch and retag it
    - buildah pull ${FULL_BASE_IMAGE_NAME}
    - buildah tag ${FULL_BASE_IMAGE_NAME} ${BASE_IMAGE_TAG}
    # Build image, squash only new layers
    - sed -i 's/FROM satellite_base_image_armhf/FROM localhost\/satellite_base_image_armhf/' audio-streamer/Containerfile_armhf
    - echo "buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm ."
    - buildah bud --layers=false -f ${IMAGE_FILE} -t ${IMAGE_TAG} --platform=linux/arm .
    - buildah tag ${IMAGE_TAG} ${FULL_IMAGE_NAME}
    - buildah images
    # Push images only from master or develop branch
    - if [[ "${CI_COMMIT_REF_SLUG}" == "master" ]] ||  [[ "${CI_COMMIT_REF_SLUG}" == "develop" ]]; then buildah push ${FULL_IMAGE_NAME}; fi
  only:
    refs:
      # Run only for important branches or if the branch name starts with the 'containerbuild' keyword,
      # else every new branch would start to build this container
      - master
      - develop
      - /^containerbuild/
    changes:
      - audio-streamer/Containerfile_armhf
      - runfiles/Containerfile_SatelliteBase_armhf
      - .gitlab-ci.yml
