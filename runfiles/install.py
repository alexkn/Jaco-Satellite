import argparse
import os
import platform
import subprocess
import sys
import time

# Load utils from jacolib without installing it locally with all dependencies
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.append(file_path + "../jacolib/jacolib/")
import utils  # noqa: E402 pylint: disable=wrong-import-position, import-error

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")


# ==================================================================================================


def pull_and_retag_image(tag, branch, namespace):
    """Download image from repository registry and retag it"""

    link = "registry.gitlab.com/{}/jaco-satellite/{}/{}"
    link = link.format(namespace, branch, tag)
    cmd = "docker pull {} && docker tag {} {}"
    cmd = cmd.format(link, link, tag)
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def download_module_containers(branch="master", namespace="jaco-assistant"):
    """Download prebuilt container images of the modules"""

    arch_l = utils.load_architecture()
    start_time = time.time()

    print("\nDownloading satellite base image ...")
    tag = "satellite_base_image_{}".format(arch_l)
    pull_and_retag_image(tag, branch, namespace)

    print("\nDownloading audio-streamer image ...")
    tag = "audio_streamer_{}".format(arch_l)
    pull_and_retag_image(tag, branch, namespace)

    print("\nDownloading wake-word image ...")
    tag = "wake_word_{}".format(arch_l)
    pull_and_retag_image(tag, branch, namespace)

    print("\nDownloading portainer image ...")
    cmd = "docker pull portainer/portainer-ce"
    subprocess.call(["/bin/bash", "-c", cmd])

    end_time = time.time()
    msg = "\nDownloading the module images took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def build_image(cf_path, tag, architecture):
    """Build squashed container image"""

    if architecture == "armhf":
        docker_platform = "linux/arm"
    else:
        docker_platform = "linux/" + architecture

    # Experimental features are required for squashing and buildx for the multiplatform builds
    cmd = "docker buildx build --platform={} --no-cache --squash -t {} - < {}"

    cmd = cmd.format(docker_platform, tag, cf_path)
    print("Running: {}".format(cmd))
    subprocess.call(["/bin/bash", "-c", cmd])


# ==================================================================================================


def build_module_containers(architecture):
    """Build container images of the modules"""

    if architecture is None and platform.machine() in ["x86_64"]:
        architecture = "amd64"
    if architecture is None and platform.machine() in ["aarch64"]:
        architecture = "arm64"
    if architecture is None and platform.machine() in ["arm", "armv7l"]:
        architecture = "armhf"
    start_time = time.time()

    print("\nBuilding satellite base image ...")
    cf_path = file_path + "Containerfile_SatelliteBase_{}".format(architecture)
    tag = "satellite_base_image_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding audio-streamer image ...")
    cf_path = file_path + "../audio-streamer/Containerfile_{}".format(architecture)
    tag = "audio_streamer_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    print("\nBuilding wake-word image ...")
    cf_path = file_path + "../wake-word/Containerfile_{}".format(architecture)
    tag = "wake_word_{}".format(architecture)
    build_image(cf_path, tag, architecture)

    end_time = time.time()
    msg = "\nBuilding the module images took {} hours\n"
    print(msg.format(utils.seconds_to_hours(end_time - start_time)))


# ==================================================================================================


def generate_modules_compose_file():
    """Generate modules compose file"""

    path = file_path + "modules-compose-template.yml"
    with open(path, "r", encoding="utf-8") as file:
        modules_compose = file.read()

    arch_l = utils.load_architecture()
    modules_compose = modules_compose.replace("{{architecture}}", arch_l)

    path = file_path + "../userdata/start-modules-compose.yml"
    with open(path, "w+", encoding="utf-8") as file:
        file.write(modules_compose)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Install Jaco-Satellite modules")
    parser.add_argument(
        "--install_modules", action="store_true", help="Install or update all modules"
    )
    parser.add_argument(
        "--build_modules", action="store_true", help="Build modules yourself"
    )
    parser.add_argument(
        "--download_modules",
        action="store_true",
        help="Download prebuilt module images",
    )
    parser.add_argument(
        "--new_compose", action="store_true", help="Rebuild start compose file"
    )
    parser.add_argument(
        "--namespace",
        action="store",
        default="jaco-assistant",
        help="Set namespace used for downloading images",
    )
    parser.add_argument(
        "--branch",
        action="store",
        default="master",
        help="Set branch used for downloading images",
    )
    parser.add_argument(
        "--architecture",
        action="store",
        choices=["amd64", "arm64", "armhf"],
        help="Set architecture used for building images",
    )
    args = parser.parse_args()

    if not any(list(vars(args).values())):
        print("Run with '--help' for argument explanations")

    if args.install_modules:
        download_module_containers(args.branch, args.namespace)
        generate_modules_compose_file()

    if args.build_modules:
        build_module_containers(args.architecture)

    if args.download_modules:
        download_module_containers(args.branch, args.namespace)

    if args.new_compose:
        generate_modules_compose_file()


# ==================================================================================================

if __name__ == "__main__":
    main()
