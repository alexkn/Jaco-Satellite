import base64
import os
import time

import numpy as np
import pvporcupine

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

output_topic = "Jaco/WakeWord"
input_topic = "Jaco/{}/AudioStream"
global_config = utils.load_global_config()

satellite_name = global_config["unique_satellite_name"].capitalize()
input_topic = input_topic.format(satellite_name)

mqtt_client = None
porcupine = None
frame_buffer = np.array([], dtype=np.int16)
max_buffer_size = 512


# ==================================================================================================


def load_keyword_listener():
    keyword = global_config["hotword"]
    possible_path = file_path + "custom-wakeword/" + keyword
    sensitivity = [global_config["hotword_sensitivity"]]

    # Check if the user has set a custom hotword
    if os.path.exists(possible_path):
        keywords = [possible_path]
        pcpn = pvporcupine.create(keyword_paths=keywords, sensitivities=sensitivity)
    else:
        keywords = [keyword]
        pcpn = pvporcupine.create(keywords=keywords, sensitivities=sensitivity)

    return pcpn


# ==================================================================================================


def on_connect(client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    global frame_buffer

    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        frames = payload["data"].encode()
        frames = base64.b64decode(frames)
        frames = np.frombuffer(frames, dtype=np.int16)
        frame_buffer = np.append(frame_buffer, frames)

        if len(frame_buffer) > max_buffer_size:
            frame_buffer = frame_buffer[-max_buffer_size:]


# ==================================================================================================


def on_keyword_detection():
    payload = {
        "data": True,
        "satellite": satellite_name,
        "timestamp": time.time(),
    }
    msg = comm_tools.encrypt_msg(payload, output_topic)
    mqtt_client.publish(output_topic, msg)
    print("Detected a wake word:", payload)


# ==================================================================================================


def main():
    global mqtt_client, porcupine, frame_buffer

    mqtt_client = comm_tools.connect_mqtt_client(on_connect, on_message)
    porcupine = load_keyword_listener()
    print("Started listening for keyword")

    while True:
        if len(frame_buffer) == max_buffer_size:
            frame = np.array(frame_buffer, dtype=np.int16)
            frame_buffer = np.array([])
            keyword_index = porcupine.process(frame)

            if keyword_index >= 0:
                on_keyword_detection()

        mqtt_client.loop(timeout=0.01)


# ==================================================================================================

if __name__ == "__main__":
    main()
