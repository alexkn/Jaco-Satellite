# Wake Word Module

[Porcupine](https://github.com/Picovoice/porcupine) is used to detect the wake word.
This readme describes how to setup and debug the Wake Word Module.

All commands are run from `Jaco-Satellite` directory.

See [link](https://github.com/Picovoice/porcupine/tree/master/resources/keyword_files/linux) for possible hotwords
(the default hotword is _"blueberry"_). You can also use different hotwords for different satellites if you like.
If you want to use a custom hotword, copy the file to `wake-word/custom-wakeword` and use the filename in the config.

## Setup

Build container:

```bash
docker build -t wake_word_amd64 - < wake-word/Containerfile_amd64
docker buildx build --platform=linux/arm64 -t wake_word_arm64 - < wake-word/Containerfile_arm64
docker buildx build --platform=linux/arm -t wake_word_armhf - < wake-word/Containerfile_armhf
```

Run the main script:

```bash
docker run --network host --rm \
  --volume `pwd`/wake-word/:/Jaco-Satellite/wake-word/:ro \
  --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  -it wake_word_amd64 python3 /Jaco-Satellite/wake-word/action-ww.py
```

## Debugging

Connect to container:

```bash
docker run --network host --rm --ipc host --device /dev/snd \
  --volume `pwd`/wake-word/:/Jaco-Satellite/wake-word/:ro \
  --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  --volume ~/.asoundrc:/etc/asound.conf:ro \
  -it wake_word_amd64
```

Test wake word:

```bash
# Run in container
pip3 install pvporcupinedemo
porcupine_demo_mic --keywords computer --audio_device_index 11
```

To test full module start audio-streamer and mqtt-broker. \
Then run the main script and speak the hotword.
