# Audio Streamer Module

This readme describes how to setup and debug the Audio Streamer Module.
It is used to stream microphone inputs and play speech outputs.

All commands are run from `Jaco-Satellite` directory.

## Setup

To setup and test your speaker and microphone there are several steps to execute.
Most of the steps are run from inside the container, that you don't have to install additional programs.

- If you're running the Satellite on a computer and the `~/.asoundrc` file is empty, copy the contents of `userdata/asound.template.conf` into it.
  If you have a raspberry pi and use the builtin sound card (through the audio jack),
  creating an empty file instead (`touch ~/.asoundrc`) should be sufficient for most audio setups. \
  If you changed the file, restart alsa with `/etc/init.d/alsa-utils restart` or log out and in again or restart your computer.

- Start the container (adjust the architecture if needed):

  ```bash
  export ARCH=amd64
  docker run --network host --rm --ipc host --device /dev/snd \
    --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
    --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
    --volume `pwd`/jacolib/:/jacolib/:ro \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    -it audio_streamer_${ARCH}
  ```

- Test your speakers:

  ```bash
  # Run in container
  speaker-test -t wav --channels 2
  ```

  If you don't hear anything, you will have to adjust your `~/.asoundrc` file. Leave the container before you change it. \
  In most cases the card id (first number, the second number is the device id) in `"hw:1,0"` is wrong. \
  Find out the correct id by running:

  ```bash
  # Run in container
  aplay --list-devices

  # You can choose a specific device like this:
  speaker-test -t wav --channels 2 --device="hw:1,0"
  ```

  If the card id is correct and you still don't hear anything, try to run the command outside the container,
  directly on your device. Also try it with an empty `~/.asoundrc` file. Sometimes restarting your computer is helpful too. \
  Else you can look [here](https://www.alsa-project.org/main/index.php/Asoundrc#The_naming_of_PCM_devices)
  for further information or try to google setups for your specific hardware devices.

- Test your microphone: \
  First, get some microphone infos.
  You need the card and device id from the first command and then the corresponding default sample rate and channel count from the second command:

  ```bash
  # Run in container
  arecord --list-devices
  python3 /Jaco-Satellite/audio-streamer/tests/print_audio_devices.py
  ```

  Record 3s of audio and listen to it afterwards. Adjust the rate and channels to your device.

  ```bash
  # Run in container
  arecord --format S16_LE --duration 3 --rate 44100 --device="hw:1,0" --channels=2 test-mic.wav
  aplay test-mic.wav
  ```

  Again, if you can't record anything, you will have to adjust your `~/.asoundrc` file.

- Find the device _index_, _maxInputChannels_ and _defaultSampleRate_ of the microphone you used above. \
   Write them into the `global_config.yaml` file:

  ```bash
  # Run in container
  python3 /Jaco-Satellite/audio-streamer/tests/print_audio_devices.py
  ```

- Now test audio recoding with python and the microphone volume:

  ```bash
  # Run in container
  python3 /Jaco-Satellite/audio-streamer/action-stream-mic.py --tofile test-mic.wav --duration 7
  aplay test-mic.wav
  ```

  When you're speaking, you should see more than a handful of the volume bars,
  else you have to make your microphone louder in the system settings.
  If you're not speaking you shouldn't see more than about three bars,
  else you might get problems with the detection of the end of your speech input.
  There's also a flag in the config file to increase or lower your microphone volume
  if you can't change it via the system settings or the volume is still to low.

  One microphone I tested had a strange sample rate problem, the recoding sample rate did not match the given default sample rate.
  The recordings were played much faster than intended and sounded like some random noise.
  If you have a similar problem, you can check this by making some repeating short sounds with a bit of silence between.
  Then test different sample rates in the `global_config.yaml` file.

  If you're on a raspi and changed your hardware setup by plugging in or out some devices while going through the above instructions,
  reboot the pi and test it again, because the microphone's _index_ might change with the first reboot.

- The last thing you have have to test here is playing audio over mqtt:

  ```bash
  # Start the mqtt broker (run this in your Jaco-Master directory)
  export ARCH=amd64
  docker run --network host --rm \
    --volume `pwd`/mqtt-broker/:/Jaco-Master/mqtt-broker/:ro \
    --volume `pwd`/jacolib/:/jacolib/:ro \
    -it mqtt_broker_${ARCH} /bin/bash -c "mosquitto -c /Jaco-Master/mqtt-broker/mosquitto.conf"

  # In a new console window start the audio player action
  export ARCH=amd64
  docker run --network host --rm --ipc host --device /dev/snd \
    --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
    --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
    --volume `pwd`/jacolib/:/jacolib/:ro \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    -it audio_streamer_${ARCH} python3 /Jaco-Satellite/audio-streamer/action-play-wavs.py

  # Send an audio file from a third console window
  export ARCH=amd64
  docker run --network host --rm --ipc host --device /dev/snd \
    --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
    --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
    --volume `pwd`/jacolib/:/jacolib/:ro \
    --volume ~/.asoundrc:/etc/asound.conf:ro \
    -it audio_streamer_${ARCH} python3 /Jaco-Satellite/audio-streamer/tests/test_play_wav.py
  ```

- If you're running this on a raspberry pi you now have to copy the `.asoundrc` file to the root user, because docker-compose loads it from there.

  ```bash
  sudo cp ~/.asoundrc /root/.asoundrc
  ```

## Debugging

Build container:

```bash
docker build -t audio_streamer_amd64 - < audio-streamer/Containerfile_amd64
docker buildx build --platform=linux/arm64 -t audio_streamer_arm64 - < audio-streamer/Containerfile_arm64
docker buildx build --platform=linux/arm -t audio_streamer_armhf - < audio-streamer/Containerfile_armhf
```

Test speakers with python:

```bash
# Run in container
# You should hear a steady sound for 10s and then the program ends itself

python3 /Jaco-Satellite/audio-streamer/tests/test_speaker_python.py \
  --output_device_index 4 --output_sample_rate 44100
```

Test microphone streaming: \
(Assumes stream-mic-action and mqtt-broker already running)

```bash
export ARCH=amd64
docker run --network host --rm --ipc host --device /dev/snd \
  --volume `pwd`/audio-streamer/:/Jaco-Satellite/audio-streamer/:ro \
  --volume `pwd`/userdata/:/Jaco-Satellite/userdata/:ro \
  --volume `pwd`/jacolib/:/jacolib/:ro \
  --volume ~/.asoundrc:/etc/asound.conf:ro \
  -it audio_streamer_${ARCH} python3 /Jaco-Satellite/audio-streamer/tests/test_play_stream.py \
    --output_device_index 4  --output_sample_rate 44100
```
