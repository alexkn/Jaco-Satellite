import argparse
import base64
import os
import time
import wave

import numpy as np
import pyaudio

import audio_utils
from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../")

sample_rate = 16000
output_topic = "Jaco/{}/AudioStream"

global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
output_topic = output_topic.format(satellite_name)

# The buffer size seems to have some influence on transcription results and wake-word detection,
# and too long buffers might result in loss of frames, reasons currently unclear
buffer_duration = 0.020
buffer_size = int(float(global_config["microphone_sample_rate"]) * buffer_duration)

client = None
audio_stream = None
audio_write_array = np.array([], dtype=np.int16)

# ==================================================================================================


def publish_audio_frame(frames, write_mode=False):
    global audio_write_array

    frames = np.frombuffer(frames, dtype=np.int16)
    frames = audio_utils.multi_to_mono_channel(
        frames, int(global_config["microphone_channels"])
    )

    msr = int(float(global_config["microphone_sample_rate"]))
    frames = audio_utils.resample(frames, msr, sample_rate)

    if global_config["filter_frequencies"]:
        frames = audio_utils.filter_audio(frames)

    frames = audio_utils.apply_volume_factor(
        frames, global_config["volume_multiplicator"]
    )
    audio_utils.print_max_volume(frames)

    if not write_mode:
        frames = base64.b64encode(frames.tostring()).decode()
        payload = {
            "data": frames,
            "timestamp": time.time(),
        }
        msg = comm_tools.encrypt_msg(payload, output_topic)
        client.publish(output_topic, msg)
    else:
        audio_write_array = np.append(audio_write_array, frames)


# ==================================================================================================


def main():
    global client, audio_stream

    parser = argparse.ArgumentParser(description="Stream file.wav to topic")
    parser.add_argument("--tofile", default="", help="Path of the file.wav to write to")
    parser.add_argument("--duration", default=3, type=int, help="Recording duration")
    args = parser.parse_args()

    pa = pyaudio.PyAudio()
    audio_stream = pa.open(
        rate=int(float(global_config["microphone_sample_rate"])),
        channels=int(global_config["microphone_channels"]),
        format=pyaudio.paInt16,
        input=True,
        frames_per_buffer=buffer_size,
        input_device_index=int(global_config["microphone_device_index"]),
    )

    if args.tofile == "":
        client = comm_tools.connect_mqtt_client(None, None)
        print("Started streaming audio")
    else:
        print("Started recording audio")

    time_start = time.time()
    while args.tofile == "" or (time.time() - time_start) < args.duration:
        frames = audio_stream.read(buffer_size, exception_on_overflow=False)
        publish_audio_frame(frames, write_mode=(args.tofile != ""))

        if args.tofile == "":
            client.loop()

    if args.tofile != "":
        wf = wave.open(args.tofile, "wb")
        wf.setnchannels(1)
        wf.setsampwidth(pyaudio.get_sample_size(pyaudio.paInt16))
        wf.setframerate(16000)
        wf.writeframes(audio_write_array.tobytes())
        wf.close()


# ==================================================================================================

if __name__ == "__main__":
    main()
