import numpy as np
import pydub
import resampy

# ==================================================================================================


def resample(frames, sample_rate_input, sample_rate_output):
    """Requires mono-channel audio input"""

    if sample_rate_input == sample_rate_output:
        return frames

    frames = frames.astype(np.float32)
    frames = resampy.resample(
        frames, sample_rate_input, sample_rate_output, filter="kaiser_fast"
    )
    frames = frames.astype(np.int16)
    return frames


# ==================================================================================================


def print_max_volume(frames):
    volume = max(0, (np.max(frames) / np.iinfo(frames.dtype).max))
    volume = int(volume * 70)
    print(" " * 80, end="\r")
    print("Volume:", "|" * volume, end="\r")


# ==================================================================================================


def multi_to_mono_channel(frames, channels):
    """
    Convert a multi channel recording to mono channel by taking the maximum of the channels

    Samples are interleaved, so for a stereo stream with left channel
    of [L0, L1, L2, ...] and right channel of [R0, R1, R2, ...],
    the output is ordered as [L0, R0, L1, R1, ...]
    """

    if channels == 1:
        return frames

    chunk_length = len(frames) / channels
    assert chunk_length == int(chunk_length)
    frames = np.reshape(frames, (int(chunk_length), channels))

    # Using absolute maximum instead of average, to ensure the audio is not silenced
    # by microphones not showing to the speaker, resulted in very noisy recordings
    frames = np.mean(frames, axis=1)
    frames = frames.astype(np.int16)

    return frames


# ==================================================================================================


def apply_volume_factor(frames, factor):
    """Apply factor to sound volume and clip values to fit in datatype range"""

    if factor == 1.0:
        return frames

    frames = frames.astype(np.float32)
    frames = frames * factor
    frames = np.clip(frames, np.iinfo(np.int16).min, np.iinfo(np.int16).max)
    frames = frames.astype(np.int16)

    return frames


# ==================================================================================================


def filter_audio(audioframes):
    """Reduce volume of frequencies above or below of normal speech"""

    audio_segment = pydub.AudioSegment(
        audioframes.tobytes(),
        frame_rate=16000,
        sample_width=audioframes.dtype.itemsize,
        channels=1,
    )

    audio_segment = audio_segment.low_pass_filter(4000)  # pylint: disable=no-member
    audio_segment = audio_segment.high_pass_filter(250)  # pylint: disable=no-member

    audioframes = np.array(audio_segment.get_array_of_samples(), dtype=np.int16)
    return audioframes
