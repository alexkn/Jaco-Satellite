import argparse
import base64
import os
import sys

import numpy as np
import pyaudio

from jacolib import comm_tools, utils

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
sys.path.insert(1, file_path + "..")
import audio_utils  # noqa: E402 pylint: disable=wrong-import-position

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../../")

sample_rate_input = 16000
input_topic = "Jaco/{}/AudioStream"
stream = None
speaker_sample_rate = None

global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
input_topic = input_topic.format(satellite_name)


# ==================================================================================================


def on_connect(client):
    client.subscribe(input_topic)


# ==================================================================================================


def on_message(client, userdata, msg):
    if msg.topic == input_topic:
        payload = comm_tools.decrypt_msg(msg.payload, msg.topic)
        frames = payload["data"].encode()
        frames = base64.b64decode(frames)
        frames = np.frombuffer(frames, dtype=np.int16)

        frames = audio_utils.resample(frames, sample_rate_input, speaker_sample_rate)
        audio_utils.print_max_volume(frames)
        frames = frames.tostring()
        stream.write(frames)


# ==================================================================================================


def main():
    global stream, speaker_sample_rate

    parser = argparse.ArgumentParser(description="Stream audio from mqtt topic")
    parser.add_argument(
        "--output_device_index",
        default=0,
        type=int,
        help="PyAudio index of the speaker",
    )
    parser.add_argument(
        "--output_sample_rate",
        default=48000,
        type=int,
        help="Sample rate of the speaker",
    )
    args = parser.parse_args()

    client = comm_tools.connect_mqtt_client(on_connect, on_message)
    speaker_sample_rate = args.output_sample_rate

    pa = pyaudio.PyAudio()
    stream = pa.open(
        rate=speaker_sample_rate,
        channels=1,
        format=pyaudio.paInt16,
        output=True,
        output_device_index=args.output_device_index,
    )

    print("Started listening to audio stream")
    client.loop_forever()


# ==================================================================================================

if __name__ == "__main__":
    main()
