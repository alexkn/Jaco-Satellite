import argparse
import time

import numpy as np
import pyaudio

# ==================================================================================================


volume = 0.3  # range [0.0, 1.0]
duration = 10.0  # in seconds, may be float
frequency = 440.0  # sine frequency, Hz, may be float

# Smaller than stream.get_write_available(), if it is to big, it may freeze the program
write_buffer_size = 1024


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Test speakers")
    parser.add_argument(
        "--output_device_index",
        default=0,
        type=int,
        help="PyAudio index of the speakers",
    )
    parser.add_argument(
        "--output_sample_rate",
        default=48000,
        type=int,
        help="Sample rate of the speakers",
    )
    args = parser.parse_args()

    # Generate samples
    samples = 2 * np.pi * np.arange(args.output_sample_rate * duration) * frequency
    samples = np.sin(samples / args.output_sample_rate)
    samples = (samples * volume).astype(np.float32)

    pa = pyaudio.PyAudio()
    # For paFloat32 sample values must be in range [-1.0, 1.0]
    stream = pa.open(
        format=pyaudio.paFloat32,
        channels=1,
        rate=args.output_sample_rate,
        output=True,
        output_device_index=args.output_device_index,
    )

    # Play the sample array in chunks
    idx = 0
    while idx < len(samples):
        old_idx = idx
        idx = min(idx + write_buffer_size, len(samples))
        buff = samples[old_idx:idx]

        while stream.get_write_available() < write_buffer_size:
            # Wait until the stream buffer is empty enough again
            time.sleep(0.001)

        stream.write(buff.tostring())

    stream.stop_stream()
    stream.close()
    pa.terminate()


# ==================================================================================================

if __name__ == "__main__":
    main()
