import argparse
import base64
import os
import time

from jacolib import comm_tools, utils

# ==================================================================================================

file_path = os.path.dirname(os.path.realpath(__file__)) + "/"
utils.set_repo_path(file_path + "../../")

output_topic = "Jaco/{}/AudioWav"
global_config = utils.load_global_config()
satellite_name = global_config["unique_satellite_name"].capitalize()
output_topic = output_topic.format(satellite_name)


# ==================================================================================================


def main():
    parser = argparse.ArgumentParser(description="Stream file.wav to topic")
    parser.add_argument("--path_wav", default="", help="Path of the file.wav to test")
    args = parser.parse_args()

    client = comm_tools.connect_mqtt_client(None, None)

    if args.path_wav == "":
        path = file_path + "asr_started.wav"
    else:
        path = args.path_wav

    with open(path, "rb") as file:
        content = file.read()

    content = base64.b64encode(content).decode()
    payload = {
        "data": content,
        "timestamp": time.time(),
        "type": "input_status",
    }
    msg = comm_tools.encrypt_msg(payload, output_topic)
    client.publish(output_topic, msg)
    client.loop()
    client.disconnect()


# ==================================================================================================

if __name__ == "__main__":
    main()
