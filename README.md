# Jaco-Satellite

Satellite nodes for Jaco Assistant.

<div align="center">
    <img src="media/jaco.png" alt="jaco logo" width="75%"/>
</div>

<br/>

[![pipeline status](https://gitlab.com/Jaco-Assistant/Jaco-Satellite/badges/master/pipeline.svg)](https://gitlab.com/Jaco-Assistant/Jaco-Satellite/-/commits/master)
[![code style black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)
[![code complexity](https://gitlab.com/Jaco-Assistant/Jaco-Satellite/-/jobs/artifacts/master/raw/badges/rcc.svg?job=analysis)](https://gitlab.com/Jaco-Assistant/Jaco-Satellite/-/commits/master)

<br/>

## Setup

Because Jaco-Satellite is using containers it should run on almost any system with one of `amd64,arm64,armhf` architectures.
Setup is tested with a Linux-Ubuntu computer and a Raspberry-Pi 4 with 4GB memory.

- Clone this repository:

  ```bash
  git clone --recurse-submodules https://gitlab.com/Jaco-Assistant/Jaco-Satellite.git
  cd Jaco-Satellite/
  ```

- Install [docker](https://docs.docker.com/engine/install/) on your satellite device.
  Afterwards enable [rootless usage](https://docs.docker.com/engine/install/linux-postinstall/#manage-docker-as-a-non-root-user) of docker for increased convenience (else you would need to prefix all `docker build/run` commands with `sudo`).

  Check the installation by running `docker run hello-world` and `docker run --rm -it ubuntu:20.04`.
  If this doesn't work, your installation is broken, please google for solutions.

- Install required python libraries:

  ```bash
  sudo apt-get install -y python3-pip
  sudo pip3 install --upgrade pyyaml docker-compose
  ```

- Set architecture in `userdata/config/global_config.template.yaml` and save as `userdata/config/global_config.yaml`. \
   Only the architecture is required right now, keep the rest as is.

- Install modules: \
  (This will download the prebuilt images, about 1.4GB for a computer or 1.2GB for a Raspberry Pi. The download itself has about half the size as the images are compressed.)

  ```bash
  python3 runfiles/install.py --install_modules
  ```

- Copy the `module_topic_keys.json` from [Jaco-Master](https://gitlab.com/Jaco-Assistant/Jaco-Master) into the `userdata` directory.

- Edit the rest of your `global_config.yaml`. \
   See readme of [audio-streamer](audio-streamer/README.md) for instructions to get the index of the audio devices (This is the complicated part of the setup). \
   And don't forget to update the _mqtt_broker_address_ if your Satellite is running on another device than the Master.

- Start modules: \
   (You have to start Jaco-Master before, or at least the mqtt-broker)

  ```bash
  docker-compose -f userdata/start-modules-compose.yml up
  ```

- Stop modules:

  ```bash
  docker-compose -f userdata/start-modules-compose.yml down
  ```

If something isn't working, please look at the readme files in the different modules for further debugging instructions, or check the source code.

<br/>

If everything did work, you can run Jaco in the background or enable it to start on bootup.

- To run it in the background, add the `--detach` flag at the end of the `docker-compose ... up` command:

  ```bash
  sleep 60; docker-compose -f userdata/start-modules-compose.yml up --detach
  ```

- To enable starting on bootup, edit `userdata/start-jaco-satellite-modules.template.service`. \
  As before save the file without the `.template` extension. Then enable and start the service:

  ```bash
  # Satellite Modules
  sudo systemctl enable `pwd`/userdata/start-jaco-satellite-modules.service
  sudo systemctl start start-jaco-satellite-modules.service

  # Check Logs
  systemctl status start-jaco-satellite-modules.service
  journalctl -u start-jaco-satellite-modules.service -e -f -b

  # Stop or remove with
  sudo systemctl stop start-jaco-satellite-modules.service
  sudo systemctl disable start-jaco-satellite-modules.service

  # Update service after file changes
  sudo systemctl daemon-reload
  ```

- If something doesn't work now, see debugging section for how to print debug logs or
  stop the services (all or only one or two) and use the `docker-compose up/down` commands to debug the error. \
   (You get more status outputs there and I haven't found a way to view them in the service logs)

<br/>

## Updating

To update the module code and containers run:

```bash
git pull && git submodule update --recursive
python3 runfiles/install.py --download_modules
```

To update everything just repeat the setup steps.

<br/>

## Debugging

Check the instructions in the readme file of the different modules. \
They contain everything which is related to their specific tasks.

<br/>

Checkout container logs or restart single containers at: http://localhost:9000/ \
After opening the link the first time, set some password and select docker in the next step.

Change the the log level of the autostart services with:

```bash
systemd-analyze get-log-level
sudo systemd-analyze set-log-level debug
```

Build base container: \
(You may have to add `--no-cache` flag if last build was run some time ago)

```bash
docker build -t satellite_base_image_amd64 - < runfiles/Containerfile_SatelliteBase_amd64
```

Enable cross-building for other architectures: \
(You have to rerun the first command after every restart of your computer)

```bash
sudo docker run --security-opt label=disable --rm --privileged multiarch/qemu-user-static --reset -p yes
docker buildx build --platform=linux/arm64 -t satellite_base_image_arm64 - < runfiles/Containerfile_SatelliteBase_arm64
docker buildx build --platform=linux/arm -t satellite_base_image_armhf - < runfiles/Containerfile_SatelliteBase_armhf
```

Build all images: \
(Building the _amd64_ images on a modern computer takes about 3&thinsp;min, about 10&thinsp;min for _arm64_, and 16&thinsp;min for all _armhf_ images)

```bash
# To squash the images, experimental features are required
sudo nano /etc/docker/daemon.json
# Insert
{
  "experimental": true
}
# Restart docker and check
sudo service docker restart
docker version

python3 runfiles/install.py --build_modules --architecture amd64
```

Run tests: \
(Check out `gitlab-ci.yml` for the steps)

```bash
docker build -t testing_jaco_sat - < tests/Containerfile

docker run --network host --rm \
  --volume `pwd`/:/Jaco-Satellite/ \
  -it testing_jaco_sat
```

Update jacolib's submodule commit:

```bash
git submodule update --remote --merge
```
